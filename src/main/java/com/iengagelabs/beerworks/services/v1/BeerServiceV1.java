package com.iengagelabs.beerworks.services.v1;

import com.iengagelabs.beerworks.web.model.v1.BeerDtoV1;

import java.util.UUID;

public interface BeerServiceV1 {
    BeerDtoV1 getBeerById(UUID beerId);

    BeerDtoV1 saveNewBeer(BeerDtoV1 beerDtoV1);

    BeerDtoV1 updateBeer(UUID beerId, BeerDtoV1 beerDtoV1);

    void deleteBeerById(UUID beerId);
}

