package com.iengagelabs.beerworks.web.model.v2;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.UUID;

public class BaseBeerDtoV2Test {

    @Autowired
    ObjectMapper objectMapper;

    BeerDtoV2 getDto() {
        BeerDtoV2 someBeerDto = BeerDtoV2.builder()
                .beerName("Galaxy Cat")
                .beerStyle(BeerStyleEnum.PALE_ALE)
                .id(UUID.randomUUID())
                .createdDate(OffsetDateTime.now())
                .lastModifiedDate(OffsetDateTime.now())
                .price(new BigDecimal("14.95"))
                .upc(123456789L)
                .build();

        return someBeerDto;
    }
}
