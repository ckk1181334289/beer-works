package com.iengagelabs.beerworks.web.model.v2;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;

@ActiveProfiles("snake")
@JsonTest
public class BeerDtoV2SnakeTest extends BaseBeerDtoV2Test {

    @Test
    void testSerializeDtoSnakeCase() throws JsonProcessingException {
        BeerDtoV2 beerDto = getDto();

        String jsonString = objectMapper.writeValueAsString(beerDto);

        System.out.println(jsonString);
    }

    @Test
    void testDeserializeDtoSnakeCase() throws IOException {

        String json = "{\"id\":\"2309ca83-3b5a-4978-b83f-b9fb9d5fa0c2\",\"beer_name\":\"Galaxy Cat\",\"beer_style\":\"PALE_ALE\",\"version\":null,\"created_date\":\"2020-01-05T06:56:05.11045-08:00\",\"last_modified_date\":\"2020-01-05T06:56:05.111958-08:00\",\"price\":14.95,\"quantity_on_hand\":null,\"upc\":123456789}\n";
        BeerDtoV2 beerDto = objectMapper.readValue(json, BeerDtoV2.class);

        System.out.println(beerDto);

    }
}
