package com.iengagelabs.beerworks.web.model.v2;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.json.JsonTest;

import java.io.IOException;

@JsonTest
class BeerDtoV2Test extends BaseBeerDtoV2Test {

    @Test
    void testSerialzeDto() throws JsonProcessingException {
        BeerDtoV2 beerDto = getDto();

        String jsonString = objectMapper.writeValueAsString(beerDto);

        System.out.println(jsonString);
    }

    @Test
    void testDeserializeDto() throws IOException {

        String json = "{\"id\":\"38555bbd-ba89-4e0d-8a65-0ef9633788bd\",\"beerName\":\"Galaxy Cat\",\"beerStyle\":\"PALE_ALE\",\"version\":null,\"createdDate\":\"2020-01-05T06:33:12.253426-08:00\",\"lastModifiedDate\":\"2020-01-05T06:33:12.254557-08:00\",\"price\":14.95,\"quantityOnHand\":null,\"upc\":123456789}";

        BeerDtoV2 beerDto = objectMapper.readValue(json, BeerDtoV2.class);

        System.out.println(beerDto);

    }
}